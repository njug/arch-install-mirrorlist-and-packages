#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

if [[ "$(tty)" = "/dev/tty1" ]]; then
	pgrep i3 || startx
fi

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

#aliases
alias i='sudo pacman -S'
alias u='sudo pacman -Syyu'
